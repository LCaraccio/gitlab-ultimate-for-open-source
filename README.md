# GitLab Ultimate or Gold for Open Source Projects

We take our responsibility of open source stewardship very seriously
(https://about.gitlab.com/2016/01/11/being-a-good-open-source-steward/ and https://about.gitlab.com/stewardship/).

GitLab exists today in large part thanks to the work of hundreds of thousands of open source contributors
around the world. To give back to this community who gives us so much, we want to help teams be
more efficient, secure, and productive. We believe the best way for them to achieve this is by
using as many of the capabilities of GitLab as possible.

It has already been the case for years that that any public project on GitLab.com gets all Gold features. We
are happy to now offer a complimentary license to GitLab Ultimate (self-hosted) or subscription to GitLab
Gold (SaaS) to all open source projects.

## Here's how to apply

1.   Create a gitlab.com account for your open source project: https://gitlab.com/users/sign_in
1.   Edit this file and add an entry to the [Open source projects using GitLab Ultimate or Gold](#open-source-projects-using-gitlab-ultimate-or-gold) 
section at the bottom of this page (all lines required):

     ```
     ### Project name
     A short description of what you do and why.
     https://myawesomeproject.org
     Ultimate or Gold?
     ```
     
1.   Commit your changes to a new branch and start a new Merge Request.

## Requirements

To apply:
- You need to be a project lead or a core contributor for an active open source project.
- Your project needs to use an [OSI-accepted open source license](https://opensource.org/licenses/alphabetical#)
- Your project must not seek to make profit from the resulting project software.

If you or your company work on commercial projects, consider our [plans for businesses](https://about.gitlab.com/pricing/). 
If you're not sure if your project meets these requirements, please [contact our support team](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334447) for help.

We'll review all requests and accept them at our discretion. If accepted, your project will be listed below and we will contact you.

## License/subscription details

- You'll receive a 1 year license for GitLab Ultimate or subscription for GitLab Gold.
- Support is not included, but can be purchased for 95% off, at $4.95/user/month. [Contact Sales](https://ultimate-free-post.about.gitlab.com/sales/) for that.
- Your license/subscription can be renewed each year if your project still meets the requirements.
   - [Contact Sales](https://ultimate-free-post.about.gitlab.com/sales/) 30 days before your license/subscription ends.
- Licenses and subscriptions cannot be transferred or sold.

## Open source projects using GitLab Ultimate or Gold

    ### GNU Mailman
    GNU Mailman is a free and open source mailing list manager. We use Gitlab for all our [development](https://github.com/mailman) and Continuous Integration.
    https://list.org
    Ultimate

    ### Manjaro Linux
    Manjaro is a user-friendly Linux distribution based on the independently developed Arch operating system. We use Gitlab for all our [development](https://gitlab.manjaro.org) and Continuous Integration.
    https://manjaro.org
    Ultimate

    ### NOC
    NOC is web-scale Operation Support System (OSS) for telecom and service providers. Areas covered by NOC include Network Resource Inventory (NRI), IP Address Management (IPAM), Fault Management (FM), Performance Management (PM), Peering Managemen. System is developed by telecom professionals and for telecom professionals.
    https://nocproject.org/
    Ultimate

    ### eelo
    An open source mobile phone OS that respects user’s data privacy
    https://www.eelo.io/
    Ultimate

    ### Mastodon
    An open source decentralized social network based on open web protocols.
    https://joinmastodon.org
    Ultimate
